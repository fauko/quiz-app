import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import 'package:logger/logger.dart';
import 'package:animated_snack_bar/animated_snack_bar.dart';

import 'package:sizer/sizer.dart';

class LoadingHelper {
  int _progress = 0;

  int get progress => _progress;

  void updateProgress(int value) {
    _progress += value;
  }
}

void snackMessenger(BuildContext context, Function setState, Function navigator,
    Widget content, String labelAction, Color colors) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    content: content,
    backgroundColor: colors,
    margin: const EdgeInsets.all(20),
    duration: const Duration(seconds: 5),
    action: SnackBarAction(
        label: labelAction,
        onPressed: () {
          setState(() {});
          navigator;
        }),
  ));
}

void pushPage(BuildContext context, Widget widget) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => widget),
  );
}

void pushReplace(BuildContext context, Widget widget) {
  Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (context) => widget),
      result: false);
}

void pushtransisiscrolAtas(BuildContext context, Widget widget) {
  Navigator.pushReplacement(
    context,
    PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) {
        return widget;
      },
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = const Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.easeInOut;
        var tween = Tween(begin: begin, end: end).chain(
          CurveTween(curve: curve),
        );
        var offsetAnimation = animation.drive(tween);
        return SlideTransition(
          position: offsetAnimation,
          child: child,
        );
      },
      transitionDuration: const Duration(milliseconds: 300),
    ),
  );
}

void pushtransisiscrolBawah(BuildContext context, Widget widget) {
  Navigator.pushReplacement(
    context,
    PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) {
        return widget;
      },
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = const Offset(0.0, -1.0); // Mengubah nilai y menjadi negatif
        var end = Offset.zero;
        var curve = Curves.easeInOut;
        var tween = Tween(begin: begin, end: end).chain(
          CurveTween(curve: curve),
        );
        var offsetAnimation = animation.drive(tween);
        return SlideTransition(
          position: offsetAnimation,
          child: child,
        );
      },
      transitionDuration: const Duration(milliseconds: 300), // Durasi animasi
    ),
  );
}

void pushtransisiscrolKanan(BuildContext context, Widget widget) {
  Navigator.pushReplacement(
    context,
    PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) {
        return widget;
      },
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = const Offset(1.0, 0.0); // Mengubah nilai x menjadi positif
        var end = Offset.zero;
        var curve = Curves.easeInOut;
        var tween = Tween(begin: begin, end: end).chain(
          CurveTween(curve: curve),
        );
        var offsetAnimation = animation.drive(tween);
        return SlideTransition(
          position: offsetAnimation,
          child: child,
        );
      },
      transitionDuration: const Duration(milliseconds: 300), // Durasi animasi
    ),
  );
}

void pushtransisiscrolKiri(BuildContext context, Widget widget) {
  Navigator.pushReplacement(
    context,
    PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) {
        return widget;
      },
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = const Offset(-1.0, 0.0); // Mengubah nilai x menjadi negatif
        var end = Offset.zero;
        var curve = Curves.easeInOut;
        var tween = Tween(begin: begin, end: end).chain(
          CurveTween(curve: curve),
        );
        var offsetAnimation = animation.drive(tween);
        return SlideTransition(
          position: offsetAnimation,
          child: child,
        );
      },
      transitionDuration: const Duration(milliseconds: 300), // Durasi animasi
    ),
  );
}

void pushPageRemove(BuildContext context, Widget widget) {
  Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => widget),
      (Route<dynamic> route) => false);
}

Future<void> exitApp() async {
  bool isDesktop() {
    return Platform.isMacOS || Platform.isWindows || Platform.isLinux;
  }

  if (isDesktop()) {
    exit(0); // desktop exit app

  } else {
    SystemNavigator.pop(); // mobile exit app

  }
}

String? validateRequire(String value, String name) {
  if (value.isEmpty) {
    return "$name harus di isi";
  }
  return null;
}

class ConvertLDR {
  static String convertToIdr(dynamic number) {
    NumberFormat currencyFormatter = NumberFormat.currency(
      locale: 'id',
      symbol: 'Rp ',
    );
    return currencyFormatter.format(number);
  }
}

void snackbarInfo(BuildContext context, String text) {
  AnimatedSnackBar.material(
    text,
    type: AnimatedSnackBarType.info,
    mobileSnackBarPosition: MobileSnackBarPosition.top,
    duration: const Duration(milliseconds: 1500),
    desktopSnackBarPosition: DesktopSnackBarPosition.topCenter,
    snackBarStrategy: RemoveSnackBarStrategy(),
  ).show(context);
}

void snackbarEror(BuildContext context, String text) {
  AnimatedSnackBar.material(
    text,
    type: AnimatedSnackBarType.error,
    mobileSnackBarPosition: MobileSnackBarPosition.bottom,
    desktopSnackBarPosition: DesktopSnackBarPosition.topCenter,
    snackBarStrategy: RemoveSnackBarStrategy(),
  ).show(context);
}

void snackbarSucces(BuildContext context, String text) {
  AnimatedSnackBar.material(
    text,
    type: AnimatedSnackBarType.success,
    mobileSnackBarPosition: MobileSnackBarPosition.top,
    desktopSnackBarPosition: DesktopSnackBarPosition.topCenter,
    snackBarStrategy: RemoveSnackBarStrategy(),
  ).show(context);
}

//kirim serial
Uint8List convertStringToUint8List(String str) {
  final List<int> codeUnits = str.codeUnits;
  final Uint8List unit8List = Uint8List.fromList(codeUnits);

  return unit8List;
}

/// terima serial
String convertUint8ListToString(Uint8List uint8list) {
  return String.fromCharCodes(uint8list);
}
