import 'package:shared_preferences/shared_preferences.dart';

prefAdmin({var email, var password}) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  var emailCon = email;
  var passCon = password;
  await preferences.setStringList('admin', [emailCon, passCon]);
}
prefUser({var name}) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  var emailCon = name;
  await preferences.setStringList('user', [emailCon]);
}


Future prefLoadAdmin() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return preferences.getStringList('admin');
}
Future prefLoadUser() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return preferences.getStringList('user');
}

Future prefRemoveAdmin() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.remove('admin');
}

Future prefRemoveUser() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.remove('user');
}
