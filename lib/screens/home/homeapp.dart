import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:logger/logger.dart';
import 'package:quiz/controller/quiz.controller.dart';
import 'package:quiz/helper.dart';
import 'package:quiz/screens/admin/admin.dart';
import 'package:quiz/screens/quiz/soal/soal.screns.dart';
import 'package:quiz/screens/quiz/topik/topik.screens.dart';
import 'package:quiz/shared_pref.dart';
import 'package:sizer/sizer.dart';

class HomeApp extends StatefulWidget {
  const HomeApp({Key? key}) : super(key: key);

  @override
  State<HomeApp> createState() => _HomeAppState();
}

class _HomeAppState extends State<HomeApp> {
  ScrollController scrolController = ScrollController();
  Future<List<Map<String, dynamic>>> quizData = getAllQuizData();

  @override
  void initState() {
    prefRemoveUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 12, 52, 85),
      body: SingleChildScrollView(
        controller: scrolController,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                  onDoubleTap: () {
                    pushtransisiscrolBawah(context, const AdminDasboard());
                  },
                  child: Container(
                    height: 10.h,
                    width: 20.w,
                    margin: EdgeInsets.only(
                      top: 5.h,
                    ),
                    color: Colors.transparent,
                  ),
                ),
              ],
            ),
            Center(
              child: Container(
                height: 20.h,
                width: 30.w,
                margin: EdgeInsets.only(top: 2.h, bottom: 20),
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/lamp.png"),
                  ),
                ),
              ),
            ),
            Text(
              "Flutter Quiz App",
              style: GoogleFonts.rubik(
                color: Colors.white,
                fontSize: 20.sp,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              "Learn - Take Quiz - Repeat",
              style: GoogleFonts.rubik(
                color: Colors.white,
                fontSize: 10.sp,
              ),
            ),
            SizedBox(
              height: 10.h,
            ),
            FutureBuilder<List<dynamic>>(
              future: quizData,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data != null) {
                    List model = snapshot.data!.toList();
                    Logger().w(model);
                    return Column(
                      children: [
                        SizedBox(
                          height: 7.h,
                          width: 70.w,
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                const Color.fromARGB(255, 51, 142, 215),
                              ), // Ubah warna latar belakang
                              overlayColor: MaterialStateProperty.all<Color>(
                                const Color.fromARGB(255, 12, 52, 85),
                              ),
                              shadowColor: MaterialStateProperty.all<Color>(
                                  const Color.fromARGB(255, 12, 52, 85)),
                              side: MaterialStateProperty.all<BorderSide>(
                                const BorderSide(
                                  width: 2,
                                  color: Color.fromARGB(255, 12, 52, 85),
                                ),
                              ),
                              surfaceTintColor:
                                  MaterialStateProperty.all<Color>(
                                      const Color.fromARGB(255, 145, 149, 154)),

                              elevation: MaterialStateProperty.all<double>(
                                  10), // Ubah elevasi
                              shape: MaterialStateProperty.all<OutlinedBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                      5.h), // Ubah border radius
                                ),
                              ),
                            ),
                            onPressed: () {
                              pushPage(
                                  context,
                                  SoalQuizScraans(
                                    quizModel: model.toList(),
                                    beforeScreens: 'home',
                                    allquiz: model,
                                    nameis: false,
                                  ));
                            },
                            child: Text(
                              "Play",
                              style: GoogleFonts.rubik(
                                fontSize: 15.sp,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 25,
                        ),
                        SizedBox(
                          height: 7.h,
                          width: 70.w,
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                const Color.fromARGB(255, 12, 52, 85),
                              ), // Ubah warna latar belakang
                              overlayColor: MaterialStateProperty.all<Color>(
                                const Color.fromARGB(255, 51, 142, 215),
                              ),
                              shadowColor: MaterialStateProperty.all<Color>(
                                  const Color.fromARGB(255, 12, 52, 85)),
                              side: MaterialStateProperty.all<BorderSide>(
                                const BorderSide(
                                  width: 2,
                                  color: Color.fromARGB(255, 51, 142, 215),
                                ),
                              ),
                              surfaceTintColor:
                                  MaterialStateProperty.all<Color>(
                                      const Color.fromARGB(255, 145, 149, 154)),

                              elevation: MaterialStateProperty.all<double>(
                                  10), // Ubah elevasi
                              shape: MaterialStateProperty.all<OutlinedBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                      5.h), // Ubah border radius
                                ),
                              ),
                            ),
                            onPressed: () {
                              pushPage(
                                  context,
                                  TopikScreensPage(
                                    quizModel: model,
                                    name: false,
                                  ));
                            },
                            child: Text(
                              "TOPICS",
                              style: GoogleFonts.rubik(
                                fontSize: 15.sp,
                                color: const Color.fromARGB(255, 51, 142, 215),
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  } else {
                    return const SizedBox();
                  }
                } else {
                  Timer.periodic(const Duration(milliseconds: 500), (timer) {
                    quizData;
                  });
                  return Text("refresh");
                }
              },
            ),
            SizedBox(
              height: 7.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  onTap: () {},
                  child: Row(
                    children: [
                      const Icon(
                        Icons.share,
                        size: 30,
                        color: Colors.blueAccent,
                      ),
                      Text(
                        "Share",
                        style: GoogleFonts.rubik(
                            fontSize: 13.sp, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {},
                  child: Row(
                    children: [
                      const Icon(
                        Icons.star,
                        size: 30,
                        color: Colors.amber,
                      ),
                      Text(
                        "Rate Us",
                        style: GoogleFonts.rubik(
                            fontSize: 13.sp, color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
