import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:logger/logger.dart';
import 'package:quiz/controller/nilai.controller.dart';
import 'package:quiz/helper.dart';
import 'package:quiz/screens/home/homeapp.dart';
import 'package:quiz/shared_pref.dart';
import 'package:sizer/sizer.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

class ReportScreensPage extends StatefulWidget {
  const ReportScreensPage(
      {Key? key,
      required this.benar,
      required this.salah,
      required this.totalsoal,
      required this.quizModel,
      required this.allquiz})
      : super(key: key);
  final int benar;
  final int salah;
  final int totalsoal;
  final List<dynamic> quizModel;
  final List<dynamic> allquiz;

  @override
  State<ReportScreensPage> createState() => _ReportScreensPageState();
}

class _ReportScreensPageState extends State<ReportScreensPage> {
  String nama = '';
  Future<void> tambahnilai() async {
    var nilai = widget.quizModel.length / widget.benar;
    await prefLoadUser().then((value) {
      setState(() {
        value != null ? nama = value[0] : nama = '';
      });
    });
    Logger().w(nilai);
    updateNilai(nama, nilai, widget.benar, widget.salah);
  }

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      tambahnilai();
    });
    Logger().w(nama);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Logger().w(widget.quizModel);
    Logger().e(widget.salah);
    Logger().i(widget.benar);
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 12, 52, 85),
      body: SizedBox(
        height: 100.h,
        width: 100.w,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 4.h, bottom: 2.h),
              height: 5.h,
              child: Row(
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 20),
                    child: GestureDetector(
                      onTap: () {
                        pushtransisiscrolKiri(context, const HomeApp());
                      },
                      child: const Icon(
                        Icons.arrow_back_ios,
                        size: 35,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 20.w, right: 20.w),
                      child: Text(
                        "Your Score",
                        style: GoogleFonts.rubik(
                          fontSize: 17.sp,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      )),
                ],
              ),
            ),
            SizedBox(
              height: 5.h,
            ),
            CircularStepProgressIndicator(
              totalSteps: 100,
              currentStep: widget.salah,
              stepSize: 10,
              selectedColor: Colors.red[200],
              unselectedColor: Colors.greenAccent,
              padding: 0,
              width: 150,
              height: 150,
              selectedStepSize: 15,
              roundedCap: (_, __) => true,
              child: Container(
                alignment: Alignment.center,
                child: Text(
                  "${widget.benar}/${widget.quizModel.length}",
                  style: GoogleFonts.rubik(
                    fontSize: 16.sp,
                    color: Colors.white,
                  ),
                ),
              ),
              //roundedCap: (index, __) => index == widget.quizModel.length,
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {},
              child: Text(
                "Share your score",
                style: GoogleFonts.rubik(
                  fontSize: 13.sp,
                  color: Colors.white,
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Text(
              "Your Report",
              style: GoogleFonts.rubik(
                fontSize: 15.sp,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 3.h,
            ),
            ListView.builder(
              shrinkWrap: true,
              itemCount: widget.quizModel.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(left: 10.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.quizModel[index]['soal'],
                        style: GoogleFonts.rubik(
                          fontSize: 13.sp,
                          color: Colors.white,
                        ),
                      ),
                      Row(
                        children: [
                          Icon(
                            widget.quizModel[index]['isian'] ==
                                    widget.quizModel[index]['jawaban']
                                ? Icons.check
                                : Icons.cancel,
                            color: widget.quizModel[index]['isian'] ==
                                    widget.quizModel[index]['jawaban']
                                ? Colors.green
                                : Colors.red,
                            size: 50,
                          ),
                          Text(
                            widget.quizModel[index]['isian'],
                            style: GoogleFonts.rubik(
                              fontSize: 13.sp,
                              color: Colors.white,
                            ),
                          ),
                          const SizedBox(
                            width: 30,
                          ),
                          Text(
                            widget.quizModel[index]['jawaban'],
                            style: GoogleFonts.rubik(
                              fontSize: 13.sp,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
