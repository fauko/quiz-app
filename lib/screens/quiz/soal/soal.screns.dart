import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:logger/logger.dart';
import 'package:quiz/screens/quiz/report.dart';
import 'package:quiz/screens/quiz/topik/topik.screens.dart';
import 'package:sizer/sizer.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import '../../../controller/nilai.controller.dart';
import '../../../helper.dart';
import '../../home/homeapp.dart';

class SoalQuizScraans extends StatefulWidget {
  const SoalQuizScraans({
    Key? key,
    required this.quizModel,
    required this.beforeScreens,
    required this.allquiz,
    required this.nameis,
  }) : super(key: key);
  final List<dynamic> quizModel;
  final List<dynamic> allquiz;
  final String beforeScreens;
  final bool nameis;
  @override
  State<SoalQuizScraans> createState() => _SoalQuizScraansState();
}

class _SoalQuizScraansState extends State<SoalQuizScraans> {
  int increment = 0;
  int salah = 0;
  int benar = 0;
  late Timer timerA;
  int waktu = 30;
  TextEditingController jawabcon = TextEditingController();
  List<dynamic> listdata = [];
  late bool isname;

  void modalInputName() {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              PersistentBottomSheetController?
                  controller; // <------ Instance variable

              TextEditingController usernamecon = TextEditingController();

              return Container(
                height: 100.h,
                //constraints: BoxConstraints(maxHeight: 90.h, maxWidth: 80.w),
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Text(
                        "Please input your name",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        margin: const EdgeInsets.all(20),
                        child: TextFormField(
                          controller: usernamecon,
                          autocorrect: false,
                          textInputAction: TextInputAction.next,
                          maxLines: 1,
                          validator: (user) {
                            if (user!.isEmpty) {
                              return "Please complete form email";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            label: Row(
                              children: [
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "Name",
                                  style: GoogleFonts.poppins(fontSize: 12.sp),
                                ),
                              ],
                            ),
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                            border: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    width: 10, color: Colors.black),
                                borderRadius: BorderRadius.circular(10)),
                          ),

                          keyboardType: TextInputType.emailAddress,
                          textAlign: TextAlign.justify,
                          // obscureText: true,
                        ),
                      ),
                      ElevatedButton(
                          onPressed: () {
                            addName(usernamecon.text);
                            setState(() {
                              isname = true;
                            });
                            Future.delayed(const Duration(milliseconds: 200),
                                () {
                              pushReplace(
                                  context,
                                  SoalQuizScraans(
                                    quizModel: widget.quizModel,
                                    allquiz: widget.allquiz,
                                    beforeScreens: widget.beforeScreens,
                                    nameis: isname,
                                  ));

                              snackbarInfo(
                                  context, "Come on, choose a quiz topic");
                            });
                          },
                          child: Text(
                            "Submit",
                            style: GoogleFonts.rubik(
                              fontSize: 14.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ))
                    ],
                  ),
                ),
              );
            },
          );
        });
  }

  @override
  void initState() {
    timerA = Timer(
        Duration(seconds: widget.beforeScreens.contains('home') ? 60 : waktu),
        () {
      timerA.cancel();
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Dialog(
              child: Container(
                  height: 70.h,
                  width: 80.w,
                  color: Colors.white,
                  child: Column(
                    children: [
                      Text(
                        "time has run out".toUpperCase(),
                        style: GoogleFonts.rubik(
                          fontSize: 16.sp,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        "Correct answer = $benar",
                        style: GoogleFonts.rubik(
                          fontSize: 16.sp,
                        ),
                      ),
                      Text(
                        "Wrong answer = $salah",
                        style: GoogleFonts.rubik(
                          fontSize: 16.sp,
                        ),
                      ),
                    ],
                  )),
            );
          });
    });
    setState(() {
      isname = widget.nameis;
    });
    isname == false
        ? Future.delayed(Duration.zero, () {
            modalInputName();
          })
        : null;

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    timerA.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color.fromARGB(255, 12, 52, 85),
      body: SafeArea(
        top: true,
        bottom: true,
        left: true,
        right: true,
        child: SizedBox(
          height: 100.h,
          width: 100.w,
          child: Column(
            children: [
              SizedBox(
                height: 5.h,
                child: Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 20),
                      child: GestureDetector(
                        onTap: () {
                          widget.beforeScreens.contains('home')
                              ? pushtransisiscrolKiri(context, const HomeApp())
                              : pushtransisiscrolKiri(
                                  context,
                                  TopikScreensPage(
                                      quizModel: widget.allquiz, name: true));
                        },
                        child: const Icon(
                          Icons.arrow_back_ios,
                          size: 35,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 20.w, right: 20.w),
                        child: Text(
                          "Quiz Page",
                          style: GoogleFonts.rubik(
                            fontSize: 17.sp,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        )),
                    Container(
                      margin: const EdgeInsets.only(left: 20),
                      child: GestureDetector(
                        onTap: () {
                          pushtransisiscrolKanan(context, const HomeApp());
                        },
                        child: Text(
                          "Exit",
                          style: GoogleFonts.rubik(
                            fontSize: 14.sp,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SingleChildScrollView(
                child: Column(
                  children: [
                    StepProgressIndicator(
                      totalSteps: widget.quizModel.length - 1,
                      currentStep: increment,
                      size: 8,
                      padding: 0,
                      selectedColor: Colors.orangeAccent,
                      unselectedColor: Colors.cyan,
                      roundedEdges: const Radius.circular(10),
                      selectedGradientColor: const LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [Colors.orangeAccent, Colors.orangeAccent],
                      ),
                      unselectedGradientColor: const LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [Colors.grey, Colors.grey],
                      ),
                    ),
                    Container(
                      height: 40.h,
                      width: 80.w,
                      margin: const EdgeInsets.only(
                          top: 10, left: 20, right: 20, bottom: 15),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 20,
                          ),
                          Text(
                            widget.quizModel[increment]['soal'],
                            style: GoogleFonts.rubik(
                                fontSize: 15.sp, color: Colors.black),
                          ),
                          const Spacer(),
                          widget.quizModel[increment]['gambarURL'] != null
                              ? Image.network(
                                  widget.quizModel[increment]['gambarURL'],
                                  height: 20.h,
                                  width: MediaQuery.of(context).size.width,
                                )
                              : const SizedBox(),
                          const SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    ),
                    widget.quizModel[increment]['tipe'] == 'pilgan' ||
                            widget.quizModel[increment]['tipe'] ==
                                ' pilihan ganda'
                        ? GestureDetector(
                            onTap: () {
                              var jum = widget.quizModel.length - 1;
                              if (increment < jum) {
                                Logger().w(jum);
                                setState(() {
                                  if (widget.quizModel[increment]['pilihan1'] ==
                                      widget.quizModel[increment]['jawaban']) {
                                    benar++;
                                    Map<String, dynamic> data = {
                                      'tipe': widget.quizModel[increment]
                                          ['tipe'],
                                      'topik': widget.quizModel[increment]
                                          ['topik'],
                                      'soal': widget.quizModel[increment]
                                          ['soal'],
                                      'isian': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan1': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan2': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan3': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan4': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'jawaban': widget.quizModel[increment]
                                          ['jawaban'],
                                    };
                                    listdata.add(data);
                                    increment++;
                                  } else {
                                    salah++;
                                    Map<String, dynamic> data = {
                                      'tipe': widget.quizModel[increment]
                                          ['tipe'],
                                      'topik': widget.quizModel[increment]
                                          ['topik'],
                                      'soal': widget.quizModel[increment]
                                          ['soal'],
                                      'isian': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan1': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan2': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan3': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan4': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'jawaban': widget.quizModel[increment]
                                          ['jawaban'],
                                    };
                                    listdata.add(data);
                                    increment++;
                                  }
                                  Future.delayed(
                                      const Duration(milliseconds: 50), () {
                                    Future.delayed(
                                        const Duration(milliseconds: 50), () {
                                      print(increment);
                                      Logger().w(increment);
                                      Logger()
                                          .w("benar = $benar , salah = $salah");
                                      print("benar = $benar , salah = $salah");
                                    });
                                  });
                                });
                              } else {
                                setState(() {
                                  if (widget.quizModel[increment]['pilihan1'] ==
                                      widget.quizModel[increment]['jawaban']) {
                                    benar++;
                                    Future.delayed(
                                        const Duration(milliseconds: 200), () {
                                      //       List<dynamic> combinedCategories = [
                                      //   ...widget.quizModel,
                                      //   ...produkList
                                      // ];
                                      Map<String, dynamic> data = {
                                        'tipe': widget.quizModel[increment]
                                            ['tipe'],
                                        'topik': widget.quizModel[increment]
                                            ['topik'],
                                        'soal': widget.quizModel[increment]
                                            ['soal'],
                                        'isian': widget.quizModel[increment]
                                            ['pilihan1'],
                                        'pilihan1': widget.quizModel[increment]
                                            ['pilihan1'],
                                        'pilihan2': widget.quizModel[increment]
                                            ['pilihan2'],
                                        'pilihan3': widget.quizModel[increment]
                                            ['pilihan3'],
                                        'pilihan4': widget.quizModel[increment]
                                            ['pilihan4'],
                                        'jawaban': widget.quizModel[increment]
                                            ['jawaban'],
                                      };
                                      listdata.add(data);
                                      Future.delayed(
                                          const Duration(milliseconds: 100),
                                          () {
                                        pushReplace(
                                            context,
                                            ReportScreensPage(
                                              allquiz: widget.allquiz,
                                              quizModel: listdata,
                                              benar: benar,
                                              salah: salah,
                                              totalsoal: listdata.length,
                                            ));
                                      });
                                    });
                                  } else {
                                    salah++;
                                    Future.delayed(
                                        const Duration(milliseconds: 200), () {
                                      //       List<dynamic> combinedCategories = [
                                      //   ...widget.quizModel,
                                      //   ...produkList
                                      // ];
                                      Map<String, dynamic> data = {
                                        'tipe': widget.quizModel[increment]
                                            ['tipe'],
                                        'topik': widget.quizModel[increment]
                                            ['topik'],
                                        'soal': widget.quizModel[increment]
                                            ['soal'],
                                        'isian': widget.quizModel[increment]
                                            ['pilihan1'],
                                        'pilihan1': widget.quizModel[increment]
                                            ['pilihan1'],
                                        'pilihan2': widget.quizModel[increment]
                                            ['pilihan2'],
                                        'pilihan3': widget.quizModel[increment]
                                            ['pilihan3'],
                                        'pilihan4': widget.quizModel[increment]
                                            ['pilihan4'],
                                        'jawaban': widget.quizModel[increment]
                                            ['jawaban'],
                                      };
                                      listdata.add(data);
                                      Future.delayed(
                                          const Duration(milliseconds: 100),
                                          () {
                                        pushReplace(
                                            context,
                                            ReportScreensPage(
                                              allquiz: widget.allquiz,
                                              quizModel: listdata,
                                              benar: benar,
                                              salah: salah,
                                              totalsoal: listdata.length,
                                            ));
                                      });
                                    });
                                  }
                                });

                                Logger().w(increment);
                              }
                            },
                            child: Container(
                              height: 7.h,
                              width: 80.w,
                              margin: const EdgeInsets.only(
                                top: 15,
                                left: 20,
                                right: 20,
                              ),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Text(
                                widget.quizModel[increment]['pilihan1'],
                                style: GoogleFonts.rubik(
                                    fontSize: 13.sp,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          )
                        : Column(
                            children: [
                              Container(
                                margin: const EdgeInsets.all(20),
                                child: TextFormField(
                                  controller: jawabcon,
                                  autocorrect: false,

                                  textInputAction: TextInputAction.next,
                                  maxLines: 5,
                                  validator: (user) {
                                    if (user!.isEmpty) {
                                      return "Please complete form esay";
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    hoverColor: Colors.white,
                                    label: Row(
                                      children: [
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          "answer",
                                          style: GoogleFonts.poppins(
                                              color: Colors.white,
                                              fontSize: 12.sp),
                                        ),
                                      ],
                                    ),
                                    contentPadding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                    fillColor: Colors.white,
                                    focusColor: Colors.white,
                                    border: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            width: 10, color: Colors.white),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                  ),

                                  keyboardType: TextInputType.emailAddress,
                                  textAlign: TextAlign.justify,
                                  // obscureText: true,
                                ),
                              ),
                              ElevatedButton(
                                  onPressed: () {
                                    var jum = widget.quizModel.length - 1;
                                    if (increment < jum) {
                                      Logger().w(jum);
                                      setState(() {
                                        if (jawabcon.text ==
                                            widget.quizModel[increment]
                                                ['jawaban']) {
                                          benar++;

                                          Map<String, dynamic> data = {
                                            'tipe': widget.quizModel[increment]
                                                ['tipe'],
                                            'topik': widget.quizModel[increment]
                                                ['topik'],
                                            'soal': widget.quizModel[increment]
                                                ['soal'],
                                            'isian': jawabcon.text,
                                            'jawaban':
                                                widget.quizModel[increment]
                                                    ['jawaban'],
                                          };
                                          listdata.add(data);
                                          increment++;

                                          Future.delayed(
                                              const Duration(milliseconds: 200),
                                              () {
                                            jawabcon.text = '';
                                            print(increment);
                                            Logger().w(increment);
                                            Logger().w(
                                                "benar = $benar , salah = $salah");
                                            print(
                                                "benar = $benar , salah = $salah");
                                          });
                                        } else {
                                          salah++;

                                          Map<String, dynamic> data = {
                                            'tipe': widget.quizModel[increment]
                                                ['tipe'],
                                            'topik': widget.quizModel[increment]
                                                ['topik'],
                                            'soal': widget.quizModel[increment]
                                                ['soal'],
                                            'isian': jawabcon.text,
                                            'jawaban':
                                                widget.quizModel[increment]
                                                    ['jawaban'],
                                          };
                                          listdata.add(data);
                                          increment++;

                                          Future.delayed(
                                              const Duration(milliseconds: 200),
                                              () {
                                            jawabcon.text = '';
                                            print(increment);
                                            Logger().w(increment);
                                            Logger().w(
                                                "benar = $benar , salah = $salah");
                                            print(
                                                "benar = $benar , salah = $salah");
                                          });
                                        }
                                      });
                                    } else {
                                      if (jawabcon.text ==
                                          widget.quizModel[increment]
                                              ['jawaban']) {
                                        benar++;
                                        Future.delayed(
                                            const Duration(milliseconds: 200),
                                            () {
                                          Map<String, dynamic> data = {
                                            'tipe': widget.quizModel[increment]
                                                ['tipe'],
                                            'topik': widget.quizModel[increment]
                                                ['topik'],
                                            'soal': widget.quizModel[increment]
                                                ['soal'],
                                            'isian': jawabcon.text,
                                            'jawaban':
                                                widget.quizModel[increment]
                                                    ['jawaban'],
                                          };
                                          listdata.add(data);
                                          Future.delayed(
                                              const Duration(milliseconds: 100),
                                              () {
                                            timerA.cancel();
                                            pushReplace(
                                                context,
                                                ReportScreensPage(
                                                  allquiz: widget.allquiz,
                                                  quizModel: listdata,
                                                  benar: benar,
                                                  salah: salah,
                                                  totalsoal: listdata.length,
                                                ));
                                          });
                                        });
                                      } else {
                                        salah++;
                                        Future.delayed(
                                            const Duration(milliseconds: 200),
                                            () {
                                          Map<String, dynamic> data = {
                                            'tipe': widget.quizModel[increment]
                                                ['tipe'],
                                            'topik': widget.quizModel[increment]
                                                ['topik'],
                                            'soal': widget.quizModel[increment]
                                                ['soal'],
                                            'isian': jawabcon.text,
                                            'jawaban':
                                                widget.quizModel[increment]
                                                    ['jawaban'],
                                          };
                                          listdata.add(data);
                                          Future.delayed(
                                              const Duration(milliseconds: 100),
                                              () {
                                            timerA.cancel();

                                            pushReplace(
                                                context,
                                                ReportScreensPage(
                                                  allquiz: widget.allquiz,
                                                  quizModel: listdata,
                                                  benar: benar,
                                                  salah: salah,
                                                  totalsoal: listdata.length,
                                                ));
                                          });
                                        });
                                      }

                                      Logger().w(increment);
                                    }
                                  },
                                  child: Text(
                                    "Next",
                                    style: GoogleFonts.rubik(
                                      fontSize: 14.sp,
                                      color: Colors.white,
                                    ),
                                  ))
                            ],
                          ),
                    //
                    widget.quizModel[increment]['tipe'] == 'pilgan' ||
                            widget.quizModel[increment]['tipe'] ==
                                ' pilihan ganda'
                        ? GestureDetector(
                            onTap: () {
                              var jum = widget.quizModel.length - 1;
                              if (increment < jum) {
                                Logger().w(jum);
                                setState(() {
                                  if (widget.quizModel[increment]['pilihan2'] ==
                                      widget.quizModel[increment]['jawaban']) {
                                    benar++;

                                    Map<String, dynamic> data = {
                                      'tipe': widget.quizModel[increment]
                                          ['tipe'],
                                      'topik': widget.quizModel[increment]
                                          ['topik'],
                                      'soal': widget.quizModel[increment]
                                          ['soal'],
                                      'isian': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan1': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan2': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan3': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan4': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'jawaban': widget.quizModel[increment]
                                          ['jawaban'],
                                    };
                                    listdata.add(data);
                                    increment++;

                                    Future.delayed(
                                        const Duration(milliseconds: 200), () {
                                      print(increment);
                                      Logger().w(increment);
                                      Logger()
                                          .w("benar = $benar , salah = $salah");
                                      print("benar = $benar , salah = $salah");
                                    });
                                  } else {
                                    salah++;

                                    Map<String, dynamic> data = {
                                      'tipe': widget.quizModel[increment]
                                          ['tipe'],
                                      'topik': widget.quizModel[increment]
                                          ['topik'],
                                      'soal': widget.quizModel[increment]
                                          ['soal'],
                                      'isian': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan1': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan2': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan3': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan4': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'jawaban': widget.quizModel[increment]
                                          ['jawaban'],
                                    };
                                    listdata.add(data);
                                    increment++;

                                    Future.delayed(
                                        const Duration(milliseconds: 200), () {
                                      print(increment);
                                      Logger().w(increment);
                                      Logger()
                                          .w("benar = $benar , salah = $salah");
                                      print("benar = $benar , salah = $salah");
                                    });
                                  }
                                });
                              } else {
                                if (widget.quizModel[increment]['pilihan2'] ==
                                    widget.quizModel[increment]['jawaban']) {
                                  benar++;
                                  Future.delayed(
                                      const Duration(milliseconds: 200), () {
                                    Map<String, dynamic> data = {
                                      'tipe': widget.quizModel[increment]
                                          ['tipe'],
                                      'topik': widget.quizModel[increment]
                                          ['topik'],
                                      'soal': widget.quizModel[increment]
                                          ['soal'],
                                      'isian': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan1': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan2': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan3': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan4': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'jawaban': widget.quizModel[increment]
                                          ['jawaban'],
                                    };
                                    listdata.add(data);
                                    Future.delayed(
                                        const Duration(milliseconds: 100), () {
                                      timerA.cancel();

                                      pushReplace(
                                          context,
                                          ReportScreensPage(
                                            allquiz: widget.allquiz,
                                            quizModel: listdata,
                                            benar: benar,
                                            salah: salah,
                                            totalsoal: listdata.length,
                                          ));
                                    });
                                  });
                                } else {
                                  salah++;
                                  Future.delayed(
                                      const Duration(milliseconds: 200), () {
                                    Map<String, dynamic> data = {
                                      'tipe': widget.quizModel[increment]
                                          ['tipe'],
                                      'topik': widget.quizModel[increment]
                                          ['topik'],
                                      'soal': widget.quizModel[increment]
                                          ['soal'],
                                      'isian': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan1': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan2': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan3': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan4': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'jawaban': widget.quizModel[increment]
                                          ['jawaban'],
                                    };
                                    listdata.add(data);
                                    Future.delayed(
                                        const Duration(milliseconds: 100), () {
                                      timerA.cancel();

                                      pushReplace(
                                          context,
                                          ReportScreensPage(
                                            allquiz: widget.allquiz,
                                            quizModel: listdata,
                                            benar: benar,
                                            salah: salah,
                                            totalsoal: listdata.length,
                                          ));
                                    });
                                  });
                                }

                                Logger().w(increment);
                              }
                            },
                            child: Container(
                              height: 7.h,
                              width: 80.w,
                              margin: const EdgeInsets.only(
                                top: 15,
                                left: 20,
                                right: 20,
                              ),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Text(
                                widget.quizModel[increment]['pilihan2'],
                                style: GoogleFonts.rubik(
                                    fontSize: 13.sp,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          )
                        : const SizedBox(),
                    //
                    widget.quizModel[increment]['tipe'] == 'pilgan' ||
                            widget.quizModel[increment]['tipe'] ==
                                ' pilihan ganda'
                        ? GestureDetector(
                            onTap: () {
                              var jum = widget.quizModel.length - 1;
                              if (increment < jum) {
                                Logger().w(jum);
                                setState(() {
                                  if (widget.quizModel[increment]['pilihan3'] ==
                                      widget.quizModel[increment]['jawaban']) {
                                    benar++;

                                    Map<String, dynamic> data = {
                                      'tipe': widget.quizModel[increment]
                                          ['tipe'],
                                      'topik': widget.quizModel[increment]
                                          ['topik'],
                                      'soal': widget.quizModel[increment]
                                          ['soal'],
                                      'isian': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan1': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan2': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan3': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan4': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'jawaban': widget.quizModel[increment]
                                          ['jawaban'],
                                    };
                                    listdata.add(data);
                                    increment++;

                                    Future.delayed(
                                        const Duration(milliseconds: 200), () {
                                      print(increment);
                                      Logger().w(increment);
                                      Logger()
                                          .w("benar = $benar , salah = $salah");
                                      print("benar = $benar , salah = $salah");
                                    });
                                  } else {
                                    salah++;

                                    Map<String, dynamic> data = {
                                      'tipe': widget.quizModel[increment]
                                          ['tipe'],
                                      'topik': widget.quizModel[increment]
                                          ['topik'],
                                      'soal': widget.quizModel[increment]
                                          ['soal'],
                                      'isian': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan1': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan2': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan3': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan4': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'jawaban': widget.quizModel[increment]
                                          ['jawaban'],
                                    };
                                    listdata.add(data);
                                    increment++;

                                    Future.delayed(
                                        const Duration(milliseconds: 200), () {
                                      print(increment);
                                      Logger().w(increment);
                                      Logger()
                                          .w("benar = $benar , salah = $salah");
                                      print("benar = $benar , salah = $salah");
                                    });
                                  }
                                });
                              } else {
                                if (widget.quizModel[increment]['pilihan3'] ==
                                    widget.quizModel[increment]['jawaban']) {
                                  benar++;
                                  Future.delayed(
                                      const Duration(milliseconds: 200), () {
                                    Map<String, dynamic> data = {
                                      'tipe': widget.quizModel[increment]
                                          ['tipe'],
                                      'topik': widget.quizModel[increment]
                                          ['topik'],
                                      'soal': widget.quizModel[increment]
                                          ['soal'],
                                      'isian': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan1': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan2': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan3': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan4': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'jawaban': widget.quizModel[increment]
                                          ['jawaban'],
                                    };
                                    listdata.add(data);
                                    Future.delayed(
                                        const Duration(milliseconds: 100), () {
                                      timerA.cancel();

                                      pushReplace(
                                          context,
                                          ReportScreensPage(
                                            allquiz: widget.allquiz,
                                            quizModel: listdata,
                                            benar: benar,
                                            salah: salah,
                                            totalsoal: listdata.length,
                                          ));
                                    });
                                  });
                                } else {
                                  salah++;
                                  Future.delayed(
                                      const Duration(milliseconds: 200), () {
                                    Map<String, dynamic> data = {
                                      'tipe': widget.quizModel[increment]
                                          ['tipe'],
                                      'topik': widget.quizModel[increment]
                                          ['topik'],
                                      'soal': widget.quizModel[increment]
                                          ['soal'],
                                      'isian': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan1': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan2': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan3': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan4': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'jawaban': widget.quizModel[increment]
                                          ['jawaban'],
                                    };
                                    listdata.add(data);
                                    Future.delayed(
                                        const Duration(milliseconds: 100), () {
                                      timerA.cancel();

                                      pushReplace(
                                          context,
                                          ReportScreensPage(
                                            allquiz: widget.allquiz,
                                            quizModel: listdata,
                                            benar: benar,
                                            salah: salah,
                                            totalsoal: listdata.length,
                                          ));
                                    });
                                  });
                                }

                                Logger().w(increment);
                              }
                            },
                            child: Container(
                              height: 7.h,
                              width: 80.w,
                              margin: const EdgeInsets.only(
                                top: 15,
                                left: 20,
                                right: 20,
                              ),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Text(
                                widget.quizModel[increment]['pilihan3'],
                                style: GoogleFonts.rubik(
                                    fontSize: 13.sp,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          )
                        : const SizedBox(),
                    //
                    widget.quizModel[increment]['tipe'] == 'pilgan' ||
                            widget.quizModel[increment]['tipe'] ==
                                ' pilihan ganda'
                        ? GestureDetector(
                            onTap: () {
                              var jum = widget.quizModel.length - 1;
                              if (increment < jum) {
                                Logger().w(jum);
                                setState(() {
                                  if (widget.quizModel[increment]['pilihan4'] ==
                                      widget.quizModel[increment]['jawaban']) {
                                    benar++;

                                    Map<String, dynamic> data = {
                                      'tipe': widget.quizModel[increment]
                                          ['tipe'],
                                      'topik': widget.quizModel[increment]
                                          ['topik'],
                                      'soal': widget.quizModel[increment]
                                          ['soal'],
                                      'isian': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'pilihan1': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan2': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan3': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan4': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'jawaban': widget.quizModel[increment]
                                          ['jawaban'],
                                    };
                                    listdata.add(data);
                                    increment++;

                                    Future.delayed(
                                        const Duration(milliseconds: 200), () {
                                      print(increment);
                                      Logger().w(increment);
                                      Logger()
                                          .w("benar = $benar , salah = $salah");
                                      print("benar = $benar , salah = $salah");
                                    });
                                  } else {
                                    salah++;

                                    Map<String, dynamic> data = {
                                      'tipe': widget.quizModel[increment]
                                          ['tipe'],
                                      'topik': widget.quizModel[increment]
                                          ['topik'],
                                      'soal': widget.quizModel[increment]
                                          ['soal'],
                                      'isian': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'pilihan1': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan2': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan3': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan4': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'jawaban': widget.quizModel[increment]
                                          ['jawaban'],
                                    };
                                    listdata.add(data);
                                    increment++;

                                    Future.delayed(
                                        const Duration(milliseconds: 200), () {
                                      print(increment);
                                      Logger().w(increment);
                                      Logger()
                                          .w("benar = $benar , salah = $salah");
                                      print("benar = $benar , salah = $salah");
                                    });
                                  }
                                });
                              } else {
                                if (widget.quizModel[increment]['pilihan4'] ==
                                    widget.quizModel[increment]['jawaban']) {
                                  benar++;
                                  Future.delayed(
                                      const Duration(milliseconds: 200), () {
                                    Map<String, dynamic> data = {
                                      'tipe': widget.quizModel[increment]
                                          ['tipe'],
                                      'topik': widget.quizModel[increment]
                                          ['topik'],
                                      'soal': widget.quizModel[increment]
                                          ['soal'],
                                      'isian': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'pilihan1': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan2': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan3': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan4': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'jawaban': widget.quizModel[increment]
                                          ['jawaban'],
                                    };
                                    listdata.add(data);
                                    Future.delayed(
                                        const Duration(milliseconds: 100), () {
                                      timerA.cancel();

                                      pushReplace(
                                          context,
                                          ReportScreensPage(
                                            allquiz: widget.allquiz,
                                            quizModel: listdata,
                                            benar: benar,
                                            salah: salah,
                                            totalsoal: listdata.length,
                                          ));
                                    });
                                  });
                                } else {
                                  salah++;
                                  Future.delayed(
                                      const Duration(milliseconds: 200), () {
                                    Map<String, dynamic> data = {
                                      'tipe': widget.quizModel[increment]
                                          ['tipe'],
                                      'topik': widget.quizModel[increment]
                                          ['topik'],
                                      'soal': widget.quizModel[increment]
                                          ['soal'],
                                      'isian': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'pilihan1': widget.quizModel[increment]
                                          ['pilihan1'],
                                      'pilihan2': widget.quizModel[increment]
                                          ['pilihan2'],
                                      'pilihan3': widget.quizModel[increment]
                                          ['pilihan3'],
                                      'pilihan4': widget.quizModel[increment]
                                          ['pilihan4'],
                                      'jawaban': widget.quizModel[increment]
                                          ['jawaban'],
                                    };
                                    listdata.add(data);
                                    Future.delayed(
                                        const Duration(milliseconds: 100), () {
                                      timerA.cancel();

                                      pushReplace(
                                          context,
                                          ReportScreensPage(
                                            allquiz: widget.allquiz,
                                            quizModel: listdata,
                                            benar: benar,
                                            salah: salah,
                                            totalsoal: listdata.length,
                                          ));
                                    });
                                  });
                                }

                                Logger().w(increment);
                              }
                            },
                            child: Container(
                              height: 7.h,
                              width: 80.w,
                              margin: const EdgeInsets.only(
                                top: 15,
                                left: 20,
                                right: 20,
                              ),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Text(
                                widget.quizModel[increment]['pilihan4'],
                                style: GoogleFonts.rubik(
                                    fontSize: 13.sp,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          )
                        : const SizedBox(),
                    //
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class StepBar extends StatelessWidget {
  final int totalSteps;
  final int currentStep;

  StepBar({required this.totalSteps, required this.currentStep});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: List.generate(
          totalSteps,
          (index) => _buildStepIndicator(index + 1),
        ),
      ),
    );
  }

  Widget _buildStepIndicator(int stepNumber) {
    bool isPassed = stepNumber <= currentStep;
    Color color = isPassed ? Colors.orangeAccent : Colors.grey;

    return Container(
      width: 30.0,
      height: 3.0, // Ubah tinggi garis sesuai keinginan Anda
      decoration: BoxDecoration(
        color: color,
      ),
      child: isPassed
          ? _buildStepNumber(stepNumber)
          : _buildIncompleteStepMarker(),
    );
  }

  Widget _buildStepNumber(int stepNumber) {
    return Container(
      width: 30.0,
      height: 30.0,
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.orangeAccent,
      ),
      child: Center(
        child: Text(
          '$stepNumber',
          style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  Widget _buildIncompleteStepMarker() {
    return Container(
      width: 10.0, // Ubah lebar marker sesuai keinginan Anda
      height: 10.0, // Ubah tinggi marker sesuai keinginan Anda
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.grey,
      ),
    );
  }
}
