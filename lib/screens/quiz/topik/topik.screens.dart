import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:quiz/controller/nilai.controller.dart';
import 'package:quiz/helper.dart';
import 'package:quiz/screens/home/homeapp.dart';
import 'package:quiz/screens/quiz/soal/soal.screns.dart';

import 'package:sizer/sizer.dart';

class TopikScreensPage extends StatefulWidget {
  const TopikScreensPage(
      {Key? key, required this.quizModel, required this.name})
      : super(key: key);
  final List<dynamic> quizModel;
  final bool name;
  @override
  State<TopikScreensPage> createState() => _TopikScreensPageState();
}

class _TopikScreensPageState extends State<TopikScreensPage> {
  late bool isname;
  void modalInputName() {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              PersistentBottomSheetController?
                  controller; // <------ Instance variable

              TextEditingController usernamecon = TextEditingController();

              return Container(
                height: 100.h,
                //constraints: BoxConstraints(maxHeight: 90.h, maxWidth: 80.w),
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Text(
                        "Please input your name",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        margin: const EdgeInsets.all(20),
                        child: TextFormField(
                          controller: usernamecon,
                          autocorrect: false,
                          textInputAction: TextInputAction.next,
                          maxLines: 1,
                          validator: (user) {
                            if (user!.isEmpty) {
                              return "Please complete form email";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            label: Row(
                              children: [
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "Name",
                                  style: GoogleFonts.poppins(fontSize: 12.sp),
                                ),
                              ],
                            ),
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                            border: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    width: 10, color: Colors.black),
                                borderRadius: BorderRadius.circular(10)),
                          ),

                          keyboardType: TextInputType.emailAddress,
                          textAlign: TextAlign.justify,
                          // obscureText: true,
                        ),
                      ),
                      ElevatedButton(
                          onPressed: () {
                            addName(usernamecon.text);
                            setState(() {
                              isname = true;
                            });
                            Future.delayed(const Duration(milliseconds: 200),
                                () {
                              pushReplace(
                                  context,
                                  TopikScreensPage(
                                      quizModel: widget.quizModel,
                                      name: isname));

                              snackbarInfo(
                                  context, "Come on, choose a quiz topic");
                            });
                          },
                          child: Text(
                            "Submit",
                            style: GoogleFonts.rubik(
                              fontSize: 14.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ))
                    ],
                  ),
                ),
              );
            },
          );
        });
  }

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      setState(() {
        isname = widget.name;
      });
      Future.delayed(const Duration(milliseconds: 200), () {
        isname == false ? modalInputName() : null;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 12, 52, 85),
      body: SafeArea(
        top: true,
        bottom: true,
        left: true,
        right: true,
        child: SizedBox(
          height: 100.h,
          width: 100.w,
          child: Column(
            children: [
              // header
              SizedBox(
                height: 5.h,
                child: Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 20),
                      child: GestureDetector(
                        onTap: () {
                          isname == false
                              ? pushtransisiscrolKanan(context, const HomeApp())
                              : showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      backgroundColor: Colors.white,
                                      title: const Text(
                                        'Want to cancel the quiz?!!',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 15,
                                        ),
                                      ),
                                      actions: [
                                        ElevatedButton(
                                          onPressed: () {
                                            pushtransisiscrolKanan(
                                                context, const HomeApp());
                                          },
                                          child: const Text(
                                            'Continue',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        )
                                      ],
                                    );
                                  });
                          ;
                        },
                        child: const Icon(
                          Icons.arrow_back_ios,
                          size: 35,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 30.w, right: 30.w),
                        child: Text(
                          "Topics",
                          style: GoogleFonts.rubik(
                            fontSize: 17.sp,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        )),
                  ],
                ),
              ),
              // body
              SizedBox(
                height: 1.h,
              ),
              ListView.builder(
                itemCount: widget.quizModel.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  // Memeriksa apakah topik telah ditambahkan sebelumnya
                  bool isAdded = false;
                  for (int i = 0; i < index; i++) {
                    if (widget.quizModel[i]['topik'] ==
                        widget.quizModel[index]['topik']) {
                      isAdded = true;
                      break;
                    }
                  }

                  // Jika topik belum ditambahkan, tambahkan ke dalam list
                  if (!isAdded) {
                    return GestureDetector(
                      onTap: () {
                        pushPage(
                          context,
                          SoalQuizScraans(
                            quizModel: widget.quizModel
                                .where((element) =>
                                    element['topik'] ==
                                    widget.quizModel[index]['topik'])
                                .toList(),
                            beforeScreens: 'topik',
                            allquiz: widget.quizModel,
                            nameis: true,
                          ),
                        );
                      },
                      child: Container(
                        height: 7.h,
                        margin:
                            const EdgeInsets.only(top: 15, left: 25, right: 25),
                        decoration: const BoxDecoration(
                          color: Color.fromARGB(255, 22, 67, 104),
                          borderRadius: BorderRadius.all(
                            Radius.circular(5),
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              margin: const EdgeInsets.only(left: 20),
                              child: Text(
                                widget.quizModel[index]['topik'],
                                style: GoogleFonts.rubik(
                                  fontSize: 14.sp,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(right: 20),
                              child: GestureDetector(
                                onTap: () {},
                                child: const Icon(
                                  Icons.navigate_next,
                                  size: 20,
                                  color: Colors.white,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  } else {
                    // Jika topik sudah ditambahkan sebelumnya, return widget kosong
                    return Container();
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
