import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:logger/logger.dart';
import 'package:quiz/controller/quiz.controller.dart';
import 'package:quiz/helper.dart';
import 'package:quiz/screens/home/homeapp.dart';
import 'package:quiz/shared_pref.dart';
import 'package:sizer/sizer.dart';

class AdminDasboard extends StatefulWidget {
  const AdminDasboard({Key? key}) : super(key: key);

  @override
  State<AdminDasboard> createState() => _AdminDasboardState();
}

class _AdminDasboardState extends State<AdminDasboard> {
  String photoUser = '';
  File file = File('');
  final picker = ImagePicker();
  imageFromGallery() async {
    var imageGallery = await picker.pickImage(source: ImageSource.gallery);
    setState(() {
      if (imageGallery != null) {
        file = File(imageGallery.path);
        print('berhasil ${imageGallery.path}');
      } else {
        print('Tidak berhasil');
      }
    });
  }

  imageFromCamera() async {
    var imageCamera = await picker.pickImage(
        source: ImageSource.camera,
        imageQuality: 100,
        maxHeight: 100,
        maxWidth: 100);
    setState(() {
      if (imageCamera != null) {
        file = File(imageCamera.path);
        print('berhasil ${imageCamera.path}');
      } else {
        print('Tidak berhasil');
      }
    });
  }

  void imagePicker(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return SafeArea(
            child: Wrap(
              children: [
                ListTile(
                  leading: const Icon(
                    Icons.library_add_sharp,
                    color: Colors.blueAccent,
                  ),
                  title: const Text(
                    'Foto dari galeri',
                    style: TextStyle(color: Colors.black),
                  ),
                  onTap: () {
                    imageFromGallery();
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  leading: const Icon(
                    Icons.camera_alt_sharp,
                    color: Colors.blueAccent,
                  ),
                  title: const Text(
                    'Ambil foto',
                    style: TextStyle(color: Colors.black),
                  ),
                  onTap: () {
                    imageFromCamera();
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          );
        });
  }

  // auth
  TextEditingController emailcon = TextEditingController();
  TextEditingController passwordcon = TextEditingController();
  bool eye = true;

  // soal
  TextEditingController tipe = TextEditingController();
  TextEditingController topik = TextEditingController();
  TextEditingController soal = TextEditingController();
  TextEditingController pilihan1 = TextEditingController();
  TextEditingController pilihan2 = TextEditingController();
  TextEditingController pilihan3 = TextEditingController();
  TextEditingController pilihan4 = TextEditingController();
  TextEditingController jawaban = TextEditingController();
  List<dynamic> listTipe = [
    {'tipe': 'esay'},
    {'tipe': 'pilgan'},
    {'tipe': 'pilihan ganda'},
  ];
  List<Widget> buildTipeSuggestions() {
    Logger().w("cek");
    return listTipe
        .where((el) => el['tipe']
            .toString()
            .toLowerCase()
            .contains(tipe.text..toString().toLowerCase()))
        .map((el) {
      return GestureDetector(
          onTap: () {
            setState(() {
              tipe.text = el['tipe'].toString();
            });
          },
          child: ListTile(
            title: Text(
              el['tipe'].toString(),
            ),
          ));
    }).toList();
  }

  @override
  void initState() {
    prefLoadAdmin().then((value) {
      setState(() {
        value != null ? emailcon.text = value[0] : emailcon.text = 'null';
        value != null ? passwordcon.text = value[1] : passwordcon.text = 'null';
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: GestureDetector(
        onTap: () async {
          await prefRemoveAdmin();
          Future.delayed(Duration.zero, () {
            pushReplace(context, const HomeApp());
          });
        },
        child: const Icon(
          Icons.logout,
          size: 50,
        ),
      )),
      body: emailcon.text != 'null'
          ? Column(
              children: [
                Expanded(
                  flex: 9,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        // tipe
                        Column(
                          children: [
                            Container(
                              margin: const EdgeInsets.all(20),
                              child: TextFormField(
                                controller: tipe,
                                autocorrect: false,
                                textInputAction: TextInputAction.done,
                                maxLines: 1,
                                onChanged: (text) {
                                  setState(
                                      () {}); // Memaksa perbaruan UI saat pengguna mengetik
                                },
                                decoration: InputDecoration(
                                  label: Row(
                                    children: [
                                      Text(
                                        "esay / pilgan",
                                        style: GoogleFonts.poppins(
                                            fontSize: 12.sp),
                                      ),
                                    ],
                                  ),
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 10),
                                  border: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          width: 10, color: Colors.black),
                                      borderRadius: BorderRadius.circular(10)),
                                ),

                                keyboardType: TextInputType.text,
                                textAlign: TextAlign.justify,
                                // obscureText: true,
                              ),
                            ),
                            if (tipe.text.isNotEmpty)
                              Container(
                                margin: const EdgeInsets.only(
                                    left: 20, right: 20, bottom: 20),
                                child: Column(
                                  children: buildTipeSuggestions(),
                                ),
                              ),
                          ],
                        ),

                        // Topik
                        Container(
                          margin: const EdgeInsets.all(20),
                          child: TextFormField(
                            controller: topik,
                            autocorrect: false,
                            textInputAction: TextInputAction.done,
                            maxLines: 1,
                            validator: (topik) {
                              if (topik!.isEmpty) {
                                return "Please complete form email";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              label: Row(
                                children: [
                                  Text(
                                    "Topik",
                                    style: GoogleFonts.poppins(fontSize: 12.sp),
                                  ),
                                ],
                              ),
                              contentPadding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 10),
                              border: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      width: 10, color: Colors.black),
                                  borderRadius: BorderRadius.circular(10)),
                            ),

                            keyboardType: TextInputType.text,
                            textAlign: TextAlign.justify,
                            // obscureText: true,
                          ),
                        ),
                        //
                        // Soal
                        Container(
                          margin: const EdgeInsets.all(20),
                          child: TextFormField(
                            controller: soal,
                            autocorrect: false,
                            textInputAction: TextInputAction.done,
                            maxLines: 6,
                            validator: (soal) {
                              if (soal!.isEmpty) {
                                return "Please complete form email";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              label: Row(
                                children: [
                                  Text(
                                    "Soal",
                                    style: GoogleFonts.poppins(fontSize: 12.sp),
                                  ),
                                ],
                              ),
                              contentPadding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 10),
                              border: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      width: 10, color: Colors.black),
                                  borderRadius: BorderRadius.circular(10)),
                            ),

                            keyboardType: TextInputType.text,
                            textAlign: TextAlign.justify,
                            // obscureText: true,
                          ),
                        ),
                        //
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            GestureDetector(
                              onTap: () {
                                imagePicker(context);
                              },
                              child: Container(
                                height: 30.h,
                                width: 60.w,
                                padding: EdgeInsets.zero,
                                margin: const EdgeInsets.only(
                                    left: 20, bottom: 20, top: 15),
                                child: file.path.isNotEmpty
                                    ? Image.file(
                                        file,
                                        fit: BoxFit.cover,
                                        width: 30.w,
                                        height: 30.h,
                                      )
                                    : const Text("Pilih Gambar"),
                              ),
                            ),
                          ],
                        ),

                        tipe.text == 'pilgan' || tipe.text == 'pilihan ganda'
                            ? Column(
                                children: [
                                  // Pilihan1
                                  Container(
                                    margin: const EdgeInsets.all(20),
                                    child: TextFormField(
                                      controller: pilihan1,
                                      autocorrect: false,
                                      textInputAction: TextInputAction.next,
                                      maxLines: 4,
                                      validator: (pilihan1) {
                                        if (pilihan1!.isEmpty) {
                                          return "Please complete form email";
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        label: Row(
                                          children: [
                                            Text(
                                              "Jawaban satu",
                                              style: GoogleFonts.poppins(
                                                  fontSize: 12.sp),
                                            ),
                                          ],
                                        ),
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 20, vertical: 10),
                                        border: OutlineInputBorder(
                                            borderSide: const BorderSide(
                                                width: 10, color: Colors.black),
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                      ),

                                      keyboardType: TextInputType.text,
                                      textAlign: TextAlign.justify,
                                      // obscureText: true,
                                    ),
                                  ),
                                  //
                                  // Pilihan2
                                  Container(
                                    margin: const EdgeInsets.all(20),
                                    child: TextFormField(
                                      controller: pilihan2,
                                      autocorrect: false,
                                      textInputAction: TextInputAction.next,
                                      maxLines: 4,
                                      validator: (pilihan2) {
                                        if (pilihan2!.isEmpty) {
                                          return "Please complete form email";
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        label: Row(
                                          children: [
                                            Text(
                                              "Jawaban dua",
                                              style: GoogleFonts.poppins(
                                                  fontSize: 12.sp),
                                            ),
                                          ],
                                        ),
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 20, vertical: 10),
                                        border: OutlineInputBorder(
                                            borderSide: const BorderSide(
                                                width: 10, color: Colors.black),
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                      ),

                                      keyboardType: TextInputType.text,
                                      textAlign: TextAlign.justify,
                                      // obscureText: true,
                                    ),
                                  ),
                                  //
                                  // Pilihan3
                                  Container(
                                    margin: const EdgeInsets.all(20),
                                    child: TextFormField(
                                      controller: pilihan3,
                                      autocorrect: false,
                                      textInputAction: TextInputAction.next,
                                      maxLines: 4,
                                      validator: (pilihan3) {
                                        if (pilihan3!.isEmpty) {
                                          return "Please complete form email";
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        label: Row(
                                          children: [
                                            Text(
                                              "Jawaban tiga",
                                              style: GoogleFonts.poppins(
                                                  fontSize: 12.sp),
                                            ),
                                          ],
                                        ),
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 20, vertical: 10),
                                        border: OutlineInputBorder(
                                            borderSide: const BorderSide(
                                                width: 10, color: Colors.black),
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                      ),

                                      keyboardType: TextInputType.text,
                                      textAlign: TextAlign.justify,
                                      // obscureText: true,
                                    ),
                                  ),
                                  //
                                  // Pilihan4
                                  Container(
                                    margin: const EdgeInsets.all(20),
                                    child: TextFormField(
                                      controller: pilihan4,
                                      autocorrect: false,
                                      textInputAction: TextInputAction.done,
                                      maxLines: 4,
                                      validator: (pilihan4) {
                                        if (pilihan4!.isEmpty) {
                                          return "Please complete form email";
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        label: Row(
                                          children: [
                                            Text(
                                              "Jawaban empat",
                                              style: GoogleFonts.poppins(
                                                  fontSize: 12.sp),
                                            ),
                                          ],
                                        ),
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 20, vertical: 10),
                                        border: OutlineInputBorder(
                                            borderSide: const BorderSide(
                                                width: 10, color: Colors.black),
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                      ),

                                      keyboardType: TextInputType.text,
                                      textAlign: TextAlign.justify,
                                      // obscureText: true,
                                    ),
                                  ),
                                  //
                                ],
                              )
                            : const SizedBox(),

                        // Hasil Jawaban
                        Container(
                          margin: const EdgeInsets.all(20),
                          child: TextFormField(
                            controller: jawaban,
                            autocorrect: false,
                            textInputAction: TextInputAction.done,
                            maxLines: 5,
                            validator: (jawaban) {
                              if (jawaban!.isEmpty) {
                                return "Please complete form email";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              label: Row(
                                children: [
                                  Text(
                                    "Hasil Jawaban",
                                    style: GoogleFonts.poppins(fontSize: 12.sp),
                                  ),
                                ],
                              ),
                              contentPadding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 10),
                              border: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      width: 10, color: Colors.black),
                                  borderRadius: BorderRadius.circular(10)),
                            ),

                            keyboardType: TextInputType.text,
                            textAlign: TextAlign.justify,
                            // obscureText: true,
                          ),
                        ),
                        //
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: SizedBox(
                    width: 60.w,
                    child: ElevatedButton(
                      onPressed: () async {
                        if (tipe.text.isNotEmpty &&
                            soal.text.isNotEmpty &&
                            jawaban.text.isNotEmpty) {
                          Logger().i("cek bro");
                          await tambahSoalr(
                              pilihan1.text,
                              pilihan2.text,
                              pilihan3.text,
                              pilihan4.text,
                              jawaban.text,
                              tipe.text,
                              topik.text,
                              soal.text,
                              file);
                        }
                        Future.delayed(Duration.zero, () {
                          pushReplace(context, const AdminDasboard());
                        });
                      },
                      child: const Text("Submit"),
                    ),
                  ),
                )
              ],
            )
          : Column(
              children: [
                // Email
                Container(
                  margin: const EdgeInsets.all(20),
                  child: TextFormField(
                    controller: emailcon,
                    autocorrect: false,
                    textInputAction: TextInputAction.next,
                    maxLines: 1,
                    validator: (email) {
                      if (email!.isEmpty) {
                        return "Please complete form email";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      label: Row(
                        children: [
                          const Image(
                            image: AssetImage("assets/mail-outline.png"),
                            height: 20,
                            width: 20,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Text(
                            "E-mail",
                            style: GoogleFonts.poppins(fontSize: 12.sp),
                          ),
                        ],
                      ),
                      prefix: const Padding(
                        padding: EdgeInsets.only(right: 20),
                        child: Image(
                          height: 20,
                          width: 20,
                          image: AssetImage("assets/mail-outline.png"),
                        ),
                      ),
                      contentPadding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      border: OutlineInputBorder(
                          borderSide:
                              const BorderSide(width: 10, color: Colors.black),
                          borderRadius: BorderRadius.circular(10)),
                    ),

                    keyboardType: TextInputType.emailAddress,
                    textAlign: TextAlign.justify,
                    // obscureText: true,
                  ),
                ),

                /// Password
                Container(
                  margin: const EdgeInsets.all(20),
                  child: TextFormField(
                    controller: passwordcon,
                    autocorrect: false,
                    textInputAction: TextInputAction.next,
                    maxLines: 1,
                    obscureText: eye,
                    validator: (password) {
                      if (password!.isEmpty) {
                        return "Please complete form password";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      label: Row(
                        children: [
                          const Image(
                            image: AssetImage("assets/password.png"),
                            height: 20,
                            width: 20,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Text(
                            "Password",
                            style: GoogleFonts.poppins(fontSize: 12.sp),
                          ),
                        ],
                      ),
                      prefix: const Padding(
                        padding: EdgeInsets.only(right: 20),
                        child: Image(
                          height: 20,
                          width: 20,
                          image: AssetImage("assets/password.png"),
                        ),
                      ),
                      suffix: Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: GestureDetector(
                          onTap: () {
                            if (eye == true) {
                              eye = false;
                            } else {
                              eye = true;
                            }
                            setState(() {});
                          },
                          child: const Icon(Icons.remove_red_eye),
                        ),
                      ),
                      contentPadding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      border: OutlineInputBorder(
                          borderSide:
                              const BorderSide(width: 10, color: Colors.black),
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    keyboardType: TextInputType.text,
                    textAlign: TextAlign.justify,
                  ),
                ),

                // Submit
                ElevatedButton(
                    onPressed: () async {
                      if (emailcon.text != 'null' ||
                          emailcon.text.isNotEmpty &&
                              passwordcon.text != 'null' ||
                          passwordcon.text.isNotEmpty) {
                        if (emailcon.text == 'admin@gmail.com' &&
                            passwordcon.text == '123') {
                          await prefAdmin(
                            email: emailcon.text,
                            password: passwordcon.text,
                          );

                          Logger().w("Login Berhasil");
                          Future.delayed(Duration.zero, () {
                            pushReplace(context, const AdminDasboard());
                          });
                        }
                      } else {
                        Logger().w("gagal login");
                      }
                    },
                    child: const Text("Login"))
              ],
            ),
    );
  }
}
