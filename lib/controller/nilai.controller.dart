import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:logger/logger.dart';
import 'package:quiz/shared_pref.dart';

Future<List<Map<String, dynamic>>> getAllNilaizData() async {
  final FirebaseFirestore firestore = FirebaseFirestore.instance;

  try {
    QuerySnapshot querySnapshot = await firestore.collection('nilai').get();

    List<Map<String, dynamic>> quizList = querySnapshot.docs.map((doc) {
      return doc.data() as Map<String, dynamic>;
    }).toList();

    return quizList;
  } catch (e) {
    print('Error getting quiz data: $e');
    return [];
  }
}

Future addName(
  String nama,
) async {
  FirebaseFirestore store = FirebaseFirestore.instance;

  if (nama.isNotEmpty) {
    Map<String, dynamic> data = {
      'nama': nama,
    };
    await prefUser(name: nama);
    var db = store.collection('nilai').doc(nama);
    db.set(data).whenComplete(() => Logger().i("berhasil"));
  }
}

Future updateNilai(
  String nama,
  double nilai,
  int benar,
  int salah,
) async {
  FirebaseFirestore store = FirebaseFirestore.instance;

  Map<String, dynamic> data = {
    'nama': nama,
    'nilai': nilai,
    'benar': benar,
    'salah': salah,
  };
  var db = store.collection('nilai').doc(nama);
  db.set(data).whenComplete(() => Logger().i("berhasil"));
}
