import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:logger/logger.dart';



Future<List<Map<String, dynamic>>> getAllQuizData() async {
  final FirebaseFirestore firestore = FirebaseFirestore.instance;

  try {
    QuerySnapshot querySnapshot = await firestore.collection('quiz').get();

    List<Map<String, dynamic>> quizList = querySnapshot.docs.map((doc) {
      return doc.data() as Map<String, dynamic>;
    }).toList();

    return quizList;
  } catch (e) {
    print('Error getting quiz data: $e');
    return [];
  }
}


Future<String?> getImageUrl(String imagePath) async {
  final FirebaseStorage storage = FirebaseStorage.instance;

  try {
    Reference storageReference = storage.ref().child(imagePath);
    String imageUrl = await storageReference.getDownloadURL();
    return imageUrl;
  } catch (e) {
    print('Error getting image URL: $e');
    return null;
  }
}

Future<void> tambahSoalr(
  String pilihan1,
  String pilihan2,
  String pilihan3,
  String pilihan4,
  String jawaban,
  String tipe,
  String topik,
  String soal,
  File imagePath,
) async {
  // Upload gambar ke Firebase Storage
  // final picker = ImagePicker();
  // final pickedFile = await picker.pickImage(source: ImageSource.gallery);
  FirebaseFirestore store = FirebaseFirestore.instance;
  FirebaseStorage storage = FirebaseStorage.instance;
  if (imagePath.path.isNotEmpty && tipe == 'pilgan' ||
      tipe == 'pilihan ganda') {
    Logger().e("test");

    //Upload gambar ke Firebase Storage
    Reference storageReference = storage.ref().child('${DateTime.now()}.png');
    UploadTask uploadTask = storageReference.putFile(File(imagePath.path));
    TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() => null);

    // Ambil URL gambar setelah diunggah
    String gambarURL = await taskSnapshot.ref.getDownloadURL();
    Logger().w(gambarURL);
    Map<String, dynamic> data = {
      'tipe': tipe,
      'topik': topik,
      'soal': soal,
      'gambarURL': gambarURL.toString(),
      'pilihan1': pilihan1,
      'pilihan2': pilihan2,
      'pilihan3': pilihan3,
      'pilihan4': pilihan4,
      'jawaban': jawaban,
    };
    // Tambahkan data ke Firestore
    Logger().e("cek bro");
    var db = store.collection('quiz').doc(soal);
    db.set(data).whenComplete(() => Logger().i("berhasil"));
  } else if (imagePath.path.isEmpty && tipe == 'pilgan' ||
      tipe == 'pilihan ganda') {
    // Tambahkan data ke Firestore
    Map<String, dynamic> data = {
      'tipe': tipe,
      'topik': topik,
      'soal': soal,
      'pilihan1': pilihan1,
      'pilihan2': pilihan2,
      'pilihan3': pilihan3,
      'pilihan4': pilihan4,
      'jawaban': jawaban,
    };
    var db = store.collection('quiz').doc(soal);
    db.set(data).whenComplete(() => Logger().i("berhasil"));
  } else if (imagePath.path.isNotEmpty && tipe == 'esay') {
    // Upload gambar ke Firebase Storage
    Reference storageReference =
        storage.ref().child('images/${DateTime.now()}.png');
    UploadTask uploadTask = storageReference.putFile(File(imagePath.path));
    TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() => null);

    // Ambil URL gambar setelah diunggah
    String gambarURL = await taskSnapshot.ref.getDownloadURL();

    // Tambahkan data ke Firestore
    Map<String, dynamic> data = {
      'tipe': tipe,
      'topik': topik,
      'soal': soal,
      'gambarURL': gambarURL.toString(),
      'jawaban': jawaban,
    };
    var db = store.collection('quiz').doc(soal);
    db.set(data).whenComplete(() => Logger().i("berhasil"));
  } else if (imagePath.path.isEmpty && tipe == 'esay') {
    Map<String, dynamic> data = {
      'tipe': tipe,
      'topik': topik,
      'soal': soal,
      'jawaban': jawaban,
    };
    var db = store.collection('quiz').doc(soal);
    db.set(data).whenComplete(() => Logger().i("berhasil"));
  }
}
