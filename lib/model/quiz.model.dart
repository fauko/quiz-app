class QuizModel {
  final String pilihan1;
  final String pilihan2;
  final String pilihan3;
  final String pilihan4;
  final String jawaban;
  final String tipe;
  final String topik;
  final String soal;
  final String imagePath;
  QuizModel({
    required this.tipe,
    required this.topik,
    required this.soal,
    required this.imagePath,
    required this.pilihan1,
    required this.pilihan2,
    required this.pilihan3,
    required this.pilihan4,
    required this.jawaban,
  });
}
